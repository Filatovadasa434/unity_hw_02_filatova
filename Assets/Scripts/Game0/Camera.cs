﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    private Vector3 set;
    public GameObject Player;
    void Start()
    {
        set = transform.position - Player.transform.position;
    }


    void LateUpdate()
    {
        transform.position = Player.transform.position + set;
    }
}
