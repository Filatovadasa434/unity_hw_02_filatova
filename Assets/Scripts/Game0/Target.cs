using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace Game0
{

[RequireComponent(typeof(CoroutineAnimation))]
public class Target : MonoBehaviour
{
  public bool is_true_target;

  CoroutineAnimation coroutine_animation;

  void Start()
  {
    coroutine_animation = GetComponent<CoroutineAnimation>();
    coroutine_animation.StartAnimation(CoroutineAnimation.Animation.SPAWN, 0.4f);
  }

  public void SelfDestroy()
  {
    coroutine_animation.StartAnimation(CoroutineAnimation.Animation.DIE, 0.4f, () => {
      GameObject.Destroy(gameObject);
    });
  }
}

}
